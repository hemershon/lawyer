// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("plugins/jquery/jquery.min").start()
require("plugins/jquery-ui/jquery-ui.min").start()
require("plugins/bootstrap/js/bootstrap.bundle.min").start()
require("plugins/chart").start()
require("plugins/Chart.min").start()
require("plugins/sparklines/sparkline").start()
require("plugins/jqvmap/jquery.vmap.min")
require("plugins/jqvmap/maps/jquery.vmap.usa").start()
require("plugins/jquery-knob/jquery.knob.min.js").start()
require("plugins/moment/moment.min.js").start()
require("daterangepicker.js").start()
require("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js").start()
require("plugins/summernote/summernote-bs4.min.js").start()
require("plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js").start()
require("dist/js/adminlte.js").start()
require("dist/js/pages/dashboard.js").start()
require("dist/js/demo.js").start()

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
